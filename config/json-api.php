<?php

return [
    'types' => [

    ],
    'mapping' => [
        'homeTeam' => 'Team',
        'awayTeam' => 'Team',
    ],
    'locale_options' => [
        'nl', 'en'
    ]
];