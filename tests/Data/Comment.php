<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 11-09-18
 * Time: 07:34
 */

namespace Stefandebruin\JsonApi\Test\Data;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Stefandebruin\JsonApi\JsonApi;

class Comment extends EloquentModel
{
    use JsonApi;

    protected $fillable = [
        'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getUrl()
    {
        // TODO: Implement getUrl() method.
    }
}
