<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 19:29
 */

namespace Stefandebruin\JsonApi\Test\Data\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Routing\Router;
use Illuminate\Contracts\Foundation\Application;

class Kernel extends HttpKernel
{
    public function __construct(Application $app, Router $router)
    {
        parent::__construct($app, $router);
        ;
    }

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'jsonApi' => \Stefandebruin\JsonApi\Middleware\JsonApi::class,
    ];
}
