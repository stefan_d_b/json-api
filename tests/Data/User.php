<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 11-09-18
 * Time: 07:34
 */

namespace Stefandebruin\JsonApi\Test\Data;

use Stefandebruin\JsonApi\Models\JsonModel;

class User extends JsonModel
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'login_count'
    ];

    protected $casts = [
        'login_count' => 'number',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * All the user posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(\Stefandebruin\JsonApi\Test\Data\Post::class, 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(\Stefandebruin\JsonApi\Test\Data\Comment::class);
    }

    public function getUrl()
    {
        return url('users/' . $this->id);
    }
}