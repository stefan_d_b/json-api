<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 11-09-18
 * Time: 20:55
 */

use Faker\Generator as Faker;

$factory->define(\Stefandebruin\JsonApi\Test\Data\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
//        'last_login' => $lastLogin,
        'login_count' => (int)$faker->biasedNumberBetween(0,100)
    ];
});

$factory->define(\Stefandebruin\JsonApi\Test\Data\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'content' => $faker->paragraph(4),
        'stars' => $faker->biasedNumberBetween(0,100),
    ];
});

$factory->afterCreating(\Stefandebruin\JsonApi\Test\Data\Post::class, function ($post, $faker) {
    $random_number_array = range(2, 20);
    shuffle($random_number_array );
    $random_number_array = array_slice($random_number_array ,0, random_int(3, 10));
    $random_number_array[] = 1;
    $post->categories()->sync($random_number_array);
});

$factory->define(\Stefandebruin\JsonApi\Test\Data\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(5),
    ];
});

$factory->define(\Stefandebruin\JsonApi\Test\Data\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
    ];
});

