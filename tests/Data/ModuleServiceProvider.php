<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 15/10/2018
 * Time: 07:36
 */

namespace Stefandebruin\JsonApi\Test\Data;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Route::get('users/{user_id}', function (Request $request, $userId) {
//            return response()->json(['status' => 'OK']);
            return new AbstractJsonApi(User::find($userId));
        });

        Route::get('users/{user_id}/relationhips/posts', function (Request $request, $userId) {
//            return response()->json(['status' => 'OK']);
            return new AbstractJsonApi(User::find($userId));
        })->name('users.relationship.posts');

        Route::get('users/{user_id}/relationhips/comments', function (Request $request, $userId) {
//            return response()->json(['status' => 'OK']);
            return new AbstractJsonApi(User::find($userId));
        })->name('users.relationship.comments');

        Route::get('posts/{post_id}', function (Request $request, $postId) {
            return new AbstractJsonApi(Post::find($postId));
        });
    }
}