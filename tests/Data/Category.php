<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 11-09-18
 * Time: 07:34
 */

namespace Stefandebruin\JsonApi\Test\Data;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Stefandebruin\JsonApi\JsonApi;
use \Stefandebruin\JsonApi\Test\Data\Post;
class Category extends EloquentModel
{
    use JsonApi;

    protected $fillable = [
        'name',
    ];

    /**
     * All the user posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function getUrl()
    {
        return url('category/' . $this->getKey());
    }
}
