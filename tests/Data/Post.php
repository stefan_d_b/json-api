<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 11-09-18
 * Time: 07:34
 */

namespace Stefandebruin\JsonApi\Test\Data;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Stefandebruin\JsonApi\JsonApi;

class Post extends EloquentModel
{
    use JsonApi;

    protected $fillable = [
        'author_id',
        'title',
        'slug',
        'content',
        'stars',
    ];

    protected $casts = [
        'stars' => 'number'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getUrl()
    {
        // TODO: Implement getUrl() method.
    }

    /**
     * All the user posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'category_post',
            'post_id',
            'category_id'
        );
    }
}
