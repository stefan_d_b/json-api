<?php

namespace Stefandebruin\JsonApi\Test;

use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Test\Data\ModuleServiceProvider;
use Stefandebruin\JsonApi\Test\Data\User;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use RefreshDatabase;

    protected $baseUrl = 'http://example.com';

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * Setup the test environment.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->withFactories(__DIR__.'/Data/database/factories');
        $this->loadMigrationsFrom(__DIR__ . '/Data/database/migrations');

        \URL::forceRootUrl('http://example.com');

        $this->faker = \Faker\Factory::create();
        $this->faker->addProvider(new \BlogArticleFaker\FakerProvider($this->faker));

        $this->bootRoutes();

        Config::set('json-api.types', [
            'users' => \Stefandebruin\JsonApi\Test\Data\User::class,
            'author' => \Stefandebruin\JsonApi\Test\Data\User::class,
            'posts' => \Stefandebruin\JsonApi\Test\Data\Post::class,
            'comments' => \Stefandebruin\JsonApi\Test\Data\Comment::class,
            'categories' => \Stefandebruin\JsonApi\Test\Data\Category::class,
        ]);
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
        $app['config']->set('database.connections.testbenchmysql', [
            'driver'    => 'mysql',
            'host'      => '127.0.01',
            'database'  => 'json_api_test',
            'username'  => 'root',
            'password'  => 'root',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ]);
    }

    protected function bootRoutes()
    {
        Route::get('testMiddleware', function () {
            return response()->json(['status' => 'OK']);
        })->name('middleware')->middleware(['jsonApi']);


        Route::get('middleware/users', function () {
            return response()->json(['status' => 'OK']);
        })->name('middleware')->middleware(['jsonApi:include,fields,sort']);

        Route::get('middleware/posts', function () {
            return response()->json(['status' => 'OK']);
        })->name('middleware')->middleware(['jsonApi:include,fields,sort']);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Stefandebruin\JsonApi\JsonApiServiceProvider',
            ModuleServiceProvider::class
        ];
    }

    /**
     * Resolve application HTTP Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function resolveApplicationHttpKernel($app)
    {
        $app->singleton('Illuminate\Contracts\Http\Kernel', 'Stefandebruin\JsonApi\Test\Data\Http\Kernel');
    }


    protected function fakeUser(){
        $gender = $this->faker->randomElements(['male', 'female']);

        $createdAt = Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', '-30 days')->getTimestamp());
        $updatedAt = Carbon::createFromFormat('Y-m-d H:i:s', $createdAt)->addDays(10);

        $lastLogin = clone $updatedAt;
        $lastLogin->addDays($this->faker->biasedNumberBetween(0,20));

        $user = new User();
        $user->fill(
            [

            ]
        );
        $user->id = $this->faker->biasedNumberBetween(0,250);
        $user->setCreatedAt($createdAt);
        $user->setUpdatedAt($updatedAt);

        $this->fakePost($user);
        return $user;
    }

    protected function fakePost(User $user){
        $createdAt = Carbon::createFromTimeStamp($this->faker->dateTimeBetween('-100 days', '-1 days')->getTimestamp());
        $updatedAt = Carbon::createFromFormat('Y-m-d H:i:s', $createdAt)->addDays(10);

        $post = new User();
        $post->fill(
            [

            ]
        );
        $post->id = $this->faker->biasedNumberBetween(0,250);
        $post->setCreatedAt($createdAt);
        $post->setUpdatedAt($updatedAt);

        $post->setRelation('author', $user);
        $collection = new Collection();
        $collection->push($post);

        $user->setRelation('posts', $collection);
        return $post;
    }
}
