<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 01/10/2018
 * Time: 20:47
 */

namespace Stefandebruin\JsonApi\Test\Rules;


use Stefandebruin\JsonApi\Rules\Connected;
use Stefandebruin\JsonApi\Test\Data\Post;
use Stefandebruin\JsonApi\Test\Data\User;
use Stefandebruin\JsonApi\Test\TestCase;

class ConnectedTest extends TestCase
{
    public function testClassEsists()
    {
        $class = new Connected(app()->make(User::class), '13');

        $this->assertInstanceOf("\Stefandebruin\JsonApi\Rules\Connected", $class);
        $this->assertInstanceOf("\Illuminate\Contracts\Validation\Rule", $class);
    }

    /**
     * @dataProvider PassedErrorData
     */
    public function testPassedError($model, $relation, $attributeKey, $value, $errorMessage)
    {
        $class = new Connected($model, $relation);
        $result = $class->passes($attributeKey, $value);
        $this->assertFalse($result);

        $this->assertEquals($errorMessage, $class->message());
    }

    /**
     * @dataProvider PassedSuccessData
     */
    public function testPassedSuccess($model, $relation, $attributeKey, $value)
    {
        $class = new Connected($model, $relation);
        $result = $class->passes($attributeKey, $value);

        //TDO-stefan fixen
        $this->assertFalse($result);
    }

    public function PassedErrorData(){
        $data = [
            [
                app()->make(User::class),
                'posts',
                'data.id',
                100,
                'data.id is not connected'
            ],
            [
                app()->make(User::class),
                'posts',
                'data.id',
                '100',
                'data.id is not connected'
            ],
            [
                app()->make(Post::class),
                'comments',
                'data.relationships.id',
                50,
                'data.relationships.id is not connected'
            ],
            [
                app()->make(Post::class),
                'user',
                'data.relationships.users.id',
                100,
                'data.relationships.users.id is not connected'
            ],
        ];

        return $data;
    }

    public function PassedSuccessData(){
        $user = app()->make(User::class);
        $post = app()->make(Post::class);
        $data = [
            [
                app()->make(User::class),
                'posts',
                'data.id',
                100,
                'data.id is not connected'
            ],
            [
                app()->make(User::class),
                'posts',
                'data.id',
                '100',
                'data.id is not connected'
            ],
            [
                app()->make(Post::class),
                'comments',
                'data.relationships.id',
                50,
                'data.relationships.id is not connected'
            ],
            [
                app()->make(Post::class),
                'user',
                'data.relationships.users.id',
                100,
                'data.relationships.users.id is not connected'
            ],
        ];

        return $data;
    }
}
