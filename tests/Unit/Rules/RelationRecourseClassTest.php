<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 01/10/2018
 * Time: 20:48
 */

namespace Stefandebruin\JsonApi\Test\Rules;


use Stefandebruin\JsonApi\Rules\RelationRecourseClass;
use Stefandebruin\JsonApi\Test\TestCase;

class RelationRecourseClassTest extends TestCase
{
    public function testClassExists()
    {
        $class = new RelationRecourseClass('users');

        $this->assertInstanceOf("\Stefandebruin\JsonApi\Rules\RelationRecourseClass", $class);
        $this->assertInstanceOf("\Illuminate\Contracts\Validation\Rule", $class);
    }

    /**
     * @dataProvider PassedErrorData
     */
    public function testPassedError($relation, $attributeKey, $value, $errorMessage)
    {
        $class = new RelationRecourseClass($relation);
        $result = $class->passes($attributeKey, $value);
        $this->assertFalse($result);
        $this->assertEquals($errorMessage, $class->message());
    }

    /**
     * @dataProvider PassedSuccessData
     */
    public function testPassedSuccess($relation, $attributeKey, $value)
    {
        $class = new RelationRecourseClass($relation);
        $result = $class->passes($attributeKey, $value);
        $this->assertTrue($result);
    }

    public function PassedErrorData()
    {
        $data = [
            [
                null,
                'data.relationsShips.users.type',
                null,
                'data.relationsShips.users.type is not a valid relation type'
            ],
            [
                'users',
                'data.relationsShips.users.type',
                null,
                'data.relationsShips.users.type is not a valid relation type'
            ],
            [
                'users',
                'data.relationsShips.users.type',
                'authors',
                'data.relationsShips.users.type is not a valid relation type'
            ],
            [
                'users',
                'data.relationsShips.users.type',
                'posts',
                'data.relationsShips.users.type is not a valid relation type'
            ],
        ];

        return $data;
    }

    public function PassedSuccessData()
    {
        $data = [
            [
                'users',
                'data.relationsShips.users.type',
                'users'
            ],
            [
                'posts',
                'data.relationsShips.users.type',
                'posts'
            ],
            [
                'users',
                'data.relationsShips.users.type',
                'author'
            ],
        ];

        return $data;
    }
}
