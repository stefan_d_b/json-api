<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 01/10/2018
 * Time: 20:48
 */

namespace Stefandebruin\JsonApi\Test\Rules;


use Stefandebruin\JsonApi\Rules\RelationType;
use Stefandebruin\JsonApi\Test\Data\User;
use Stefandebruin\JsonApi\Test\TestCase;

class RelationTypeTest extends TestCase
{
    public function testClassExists()
    {
        $class = new RelationType(app()->make(User::class), 'posts');

        $this->assertInstanceOf("\Stefandebruin\JsonApi\Rules\RelationType", $class);
        $this->assertInstanceOf("\Illuminate\Contracts\Validation\Rule", $class);
    }
}
