<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 17/10/2018
 * Time: 07:46
 */

namespace Stefandebruin\JsonApi\Test\Rules;

use Stefandebruin\JsonApi\Rules\RequiredIfMethod;
use Stefandebruin\JsonApi\Test\TestCase;

class RequiredIfMethodTest extends TestCase
{
    public function testClassExists()
    {
        $class = new RequiredIfMethod('GET', []);

        $this->assertInstanceOf("\Stefandebruin\JsonApi\Rules\RequiredIfMethod", $class);
        $this->assertInstanceOf("\Illuminate\Contracts\Validation\Rule", $class);
    }
}
