<?php namespace Stefandebruin\JsonApi\Test\Unit\Service;

use Stefandebruin\JsonApi\Test\TestCase;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-09-18
 * Time: 07:34
 */
class JsonApiTest extends TestCase
{
    /**
     *
     */
    public function testFacadeBinding()
    {
        $this->assertEquals('Stefandebruin\JsonApi\Service\JsonApi', get_class(app()->make('json-api')));
    }

    /**
     *
     */
    public function testGetResourceType()
    {
        $this->assertTrue(true);
    }

    /**
     *
     */
    public function testGetResourceClass()
    {
        $this->assertTrue(true);
    }

    public function testGetResourceTypeByClass()
    {
        $this->assertTrue(true);
    }
}
