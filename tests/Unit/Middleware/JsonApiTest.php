<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 19:34
 */

namespace Stefandebruin\JsonApi\Test\Unit\Middleware;

use Illuminate\Support\Facades\Config;
use Stefandebruin\JsonApi\Exceptions\BadIncludeException;
use Stefandebruin\JsonApi\Test\TestCase;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class JsonApiTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp()
    {
        parent::setUp();
    }

    public function testRequiredHeadersException()
    {
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->json('get', 'testMiddleware');

        $response
            ->assertStatus(415);
        $this->assertequals(
            get_class($response->exception),
            'Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException'
        );
    }

    public function testRequiredHeaders()
    {
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/testMiddleware');

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');
    }

    public function testGetExceptionByGetParameter()
    {
        $parameters = ['include', 'sort'];

        foreach ($parameters as $parameter) {
            /** @var \Illuminate\Foundation\Testing\TestResponse $response */
            $response = $this->withHeaders($this->getHeaders())->get('/testMiddleware?' . $parameter . '=value');

            $response
                ->assertStatus(400);
            $this->assertequals(
                get_class($response->exception),
                BadRequestHttpException::class
            );
        }
    }

    public function testIncludeParameter()
    {

        //test specifieke url op simpele manier
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?include=posts');
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');


        //Test specifieke url op uitgebreide manier ( met sub include)
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?include=posts.comments');
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');


        //Test specifieke url met niet bestaande relatie
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?include=reactions');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadIncludeException::class
        );

        //invald character
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?include=posts.comm-ents');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );
        $this->assertequals(
            "invalid characters used",
            $response->exception->getMessage()
        );
    }

    public function testFieldsParameter(){
        //test specifieke url op simpele manier
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?fields[posts]=label');
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');


        //Test specifieke url met niet bestaande relatie
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?fields[pages]=title');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );


        //Test specifieke url met niet bestaande relatie
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?fields[posts]=ti+tle');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );
        $this->assertequals(
            "invalid characters used",
            $response->exception->getMessage()
        );

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?fields[pos+ts]=ti+tle');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );
        $this->assertequals(
            "invalid characters used",
            $response->exception->getMessage()
        );
    }

    public function testSortParameter(){
        //test specifieke url op simpele manier
        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
//        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?sort=name');
//        $response
//            ->assertStatus(200)
//            ->assertJson(['status' => 'OK'])
//            ->assertHeader('accept', 'application/vnd.api+json');


        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?sort=nam$!e');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );
        $this->assertequals(
            "invalid characters used",
            $response->exception->getMessage()
        );

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('/middleware/users?sort=-name');
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');
    }

    public function testLocaleOption(){
        Config::set('json-api.locale_options', ['nl','fr','en']);

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('testMiddleware?locale=nl');
        $this->assertEquals('nl', \config('app.locale'));
        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'OK'])
            ->assertHeader('accept', 'application/vnd.api+json');

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->withHeaders($this->getHeaders())->get('testMiddleware?locale=de');
        $response
            ->assertStatus(400);
        $this->assertequals(
            get_class($response->exception),
            BadRequestHttpException::class
        );
        $this->assertequals(
            $response->exception->getMessage(),
            "Language not configure"
        );
    }

    private function getHeaders()
    {
        return [
            'Content-Type' => 'application/vnd.api+json',
            'Accept' => 'application/vnd.api+json',
        ];
    }
}
