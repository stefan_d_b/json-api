<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-09-18
 * Time: 21:22
 */

namespace Stefandebruin\JsonApi\Test\Unit\Resources;

use Illuminate\Support\Facades\Route;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;
use Stefandebruin\JsonApi\Test\Data\Category;
use Stefandebruin\JsonApi\Test\Data\Post;
use Stefandebruin\JsonApi\Test\Data\User;
use Stefandebruin\JsonApi\Test\TestCase;

class AbstractJsonApiTest extends TestCase
{

    /**
     * @dataProvider responseData
     * @param $url
     * @param $relationsStructure
     * @param $resourceType
     * @param $entityId
     */
    public function testResponse($url, $resourceType, $entityId, $relationsStructure)
    {
        factory(Category::class, 20)->create();
        factory(User::class, 10)->create()->each(function ($u) {
            /** @var User $u */
            for ($i = 1; $i <= random_int(1, 10); $i++) {
                $u->posts()->save(
                    factory(Post::class)->create()
                );
            }
        });


        $responseStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [

                ],
                'links' => [
                    'self'
                ],
                'relationships' => $relationsStructure
            ]
        ];

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->json('GET', $url);
        if($response->getStatusCode() !== 200){
            dd($response);
        }
        $response
            ->assertJsonFragment(['type' => $resourceType])
            ->assertJsonFragment(['id' => $entityId])
            ->assertJsonStructure($responseStructure)
            ->assertStatus(200);

        $this->assertJsonRelationships($response);
    }

    public function responseData()
    {
        return [
            [
                'users/1',
                'users',
                '1',
                [
                    'posts' => [
                        'links' => [

                        ],
                        'data' => [
                            '*' => [
                                'type',
                                'id'
                            ]
                        ]
                    ],
                    'comments' => [
                        'links' => [

                        ],
                        'data' => [
                            '*' => [
                                'type',
                                'id'
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }


    protected function bootRoutes()
    {
        parent::bootRoutes();
    }

    public function assertJsonRelationships(\Illuminate\Foundation\Testing\TestResponse $response)
    {
        $relationShips = $response->json('data.relationships');

    }
}
