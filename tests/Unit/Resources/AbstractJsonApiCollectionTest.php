<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 10-09-18
 * Time: 21:22
 */

namespace Unit\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Stefandebruin\JsonApi\Resources\AbstractJsonApiCollection;
use Stefandebruin\JsonApi\Test\TestCase;

class TestAbstractJsonApiCollection extends TestCase
{
    public function testClassExist()
    {
        $class = new AbstractJsonApiCollection(collect());
        $this->assertInstanceOf(ResourceCollection::class, $class);
    }
}
