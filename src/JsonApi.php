<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 25-03-18
 * Time: 20:11
 */

namespace Stefandebruin\JsonApi;

use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;

trait JsonApi
{


    public function getResourceClass()
    {
        return property_exists($this, 'resource') ? $this->resource : null;
    }

    abstract public function getUrl();

    public function relationships()
    {

        $model = new static;

        $relationships = [];

        try {
            foreach ((new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                if ($method->class != get_class($model) ||
                    !empty($method->getParameters()) ||
                    $method->getName() == __FUNCTION__) {
                    continue;
                }

                $return = $method->invoke($model);

                if ($return instanceof Relation) {
                    $relationships[$method->getName()] = [
                        'type' => (new ReflectionClass($return))->getShortName(),
                        'model' => (new ReflectionClass($return->getRelated()))->getName()
                    ];
                }
            }
        } catch (\ReflectionException $e) {
        }
        return $relationships;
    }
}
