<?php namespace Stefandebruin\JsonApi;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 25-03-18
 * Time: 11:49
 */

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Service\FilterCollectionService;
use Stefandebruin\JsonApi\Service\JsonApi as JsonApiService;
use Stefandebruin\JsonApi\Service\ModelInformation;

class JsonApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('testValidation', function ($attribute, $value, $parameters, $validator) {
            /** @var ModelInformation $modelInformation */
            $modelInformation = app()->make(ModelInformation::class);
            $resource = JsonApi::getResourceType();
            $resourceClass = JsonApi::getResourceClass($resource);
            $relation = str_replace('data.relationships.', '', $attribute);

            return array_has($modelInformation->relations($resourceClass), $relation);
        });

        Validator::extend('resourceId', function ($attribute, $value, $parameters, $validator) {
            $split = explode('/', request()->path());

            return $value === end($split);
        });

        $this->publishes([
            __DIR__.'/../path/json-api.php' => config_path('json-api.php'),
        ]);

        $this->mergeConfigFrom(
            __DIR__.'/../config/json-api.php',
            'json-api'
        );

        Validator::extend('notAllowed', function ($attribute, $value, $parameters, $validator) {
            return false;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('json-api', JsonApiService::class);
        $this->app->bind('collectionFilter', FilterCollectionService::class);
    }
}
