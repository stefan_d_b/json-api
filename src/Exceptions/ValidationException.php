<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 22-09-18
 * Time: 14:17
 */

namespace Stefandebruin\JsonApi\Exceptions;

use \Illuminate\Validation\ValidationException as IlluminateValidationException;

class ValidationException extends IlluminateValidationException
{
    /**
     * Get all of the validation error messages.
     *
     * @return array
     */
    public function errors()
    {
        $errors = [];

        foreach ($this->validator->errors()->messages() as $field => $messages) {
            foreach ($messages as $message) {
                $errors[] = [
                    'status' => '422',
                    'source' => ['pointer' => '/' . str_replace('.', '/', $field)],
                    'title' => $this->formatMessage($message),
                ];
            }
        }
        return $errors;
    }

    private function formatMessage($message)
    {
        return str_replace(['data.', 'attributes.'], '', $message);
    }
}
