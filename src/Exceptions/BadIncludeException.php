<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 29-03-18
 * Time: 19:29
 */

namespace Stefandebruin\JsonApi\Exceptions;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BadIncludeException extends BadRequestHttpException
{
    /**
     * @var array
     */
    private $source;
    /**
     * @var string
     */
    private $title;

    public function __construct(
        array $source = [],
        String $title = null,
        String $message = null,
        \Exception $previous = null,
        $code = 0
    ) {
        parent::__construct($message, $previous, $code);
        $this->source = $source;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): String
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getSource(): array
    {
        return $this->source;
    }
}
