<?php namespace Stefandebruin\JsonApi\Controllers;

/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 14/10/2018
 * Time: 14:18
 */

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Controller as BaseController;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Resources\AbstractJsonApi;

class JsonController extends BaseController
{
    /**
     * @param FormRequest $request
     * @return mixed
     */
    public function createModel(FormRequest $request)
    {
        $model = app()->make(JsonApi::getResourceClass($request->input('data.type')));
        $model->fill($request->input('data.attributes'));
        $resourceClass = $model->getResourceClass() ?? AbstractJsonApi::class;
        $model->save();

        \Bouncer::allow(\Auth::user())->toOwn($model);

        return new $resourceClass($model);
    }
}
