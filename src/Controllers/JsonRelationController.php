<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 14/10/2018
 * Time: 20:12
 */

namespace Stefandebruin\JsonApi\Controllers;

use Illuminate\Http\Request;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Requests\JsonApiRelationRequest;
use Stefandebruin\JsonApi\Resources\JsonRelationShipCollection;

class JsonRelationController extends JsonController
{
    public function showRelation(Request $request)
    {
        $info = $this->getInfo($request);
        $model = app()->make(JsonApi::getResourceClass($info['resourceType']))->find($info['itemId']);

        $relationKey = $info['relationKey'];
        return new JsonRelationShipCollection($model->$relationKey);
    }

    public function updateRelation(JsonApiRelationRequest $request)
    {
        $info = $this->getInfo($request);
        $model = app()->make(JsonApi::getResourceClass($info['resourceType']))->find($info['itemId']);

        $relationKey = $info['relationKey'];
        if ($request->getMethod() === "DELETE") {
            foreach ($request->json('data') as $item) {
                $model->$relationKey()->detach($item['id']);
            }
        } else {
            if (count($request->json('data')) === 0) {
                $model->$relationKey()->sync([]);
            } else {
                $items = $model->$relationKey;
                foreach ($request->json('data') as $item) {
                    if (!in_array($item['id'], $items->pluck('id')->toArray())) {
                        $model->$relationKey()->attach($item['id'], array_get($item, 'attributes', []));
                    }
                }
            }
        }
        return response()->json()->setStatusCode(204);
    }


    private function getInfo($request)
    {
        $path = $request->getPathInfo();
        list($model, $relationKey) = explode('relationships', $path);
        $info = explode('/', $model);
        $info = array_reverse(array_filter($info));
        return ['itemId' => $info[0], 'resourceType' => $info[1], 'relationKey' => str_replace('/', '', $relationKey)];
    }
}
