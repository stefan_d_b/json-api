<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 09-09-18
 * Time: 21:24
 */

namespace Stefandebruin\JsonApi\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class JsonApi
 * @method static getResourceType($path = null, $delimiter = '/')
 * @method static getResourceClass($resource)
 * @method static getResourceTypeByClass($class)
 * @package Stefandebruin\JsonApi\Facades
 */
class JsonApi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'json-api';
    }
}
