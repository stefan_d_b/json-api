<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 27-09-18
 * Time: 19:12
 */

namespace Stefandebruin\JsonApi\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class JsonModel extends EloquentModel
{
    protected $hiddenRelations = [];

    /**
     * @return array
     */
    public function getHiddenRelations()
    {
        return $this->hiddenRelations;
    }

//    protected $dateFormat = 'Y-m-d\TH:i:sP';
}
