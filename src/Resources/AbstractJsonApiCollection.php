<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 25-03-18
 * Time: 19:40
 */

namespace Stefandebruin\JsonApi\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Str;

class AbstractJsonApiCollection extends ResourceCollection
{
    /**
     * @var array
     */
    private $includes = [];

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->includes = [];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $links = [
            'self' => $request->url()
        ];

        foreach ($this->getIncludeRelations($request) as $relation) {
            $links[$relation] = $request->url() . '/' . $relation;
        }

        return [
            'data' => $this->collection,
            'links' => $links,
            'user-check' => \Auth::user(),
        ];
    }

    public function with($request)
    {
        /** @var \Illuminate\Support\Collection $itemCollection */
        $itemCollection = $this->collection->pluck('resource');

        $this->mapCollectionForRelations($itemCollection, $this->getIncludeRelations($request));

//        return [];
        return count($this->includes) > 0 ? ['included' => array_values($this->includes)] : [];
    }

    private function getIncludeRelations($request)
    {
        return array_filter(
            explode(
                ",",
                $request->get('include')
            ),
            function ($value) {
                return !empty($value);
            }
        );
    }

    /**
     * Get the resource that this resource collects.
     *
     * @return string|null
     */
    protected function collects()
    {
        $collect = null;
        if ($this->collects) {
            $collect = $this->collects;
        }

        if ($this->resource->count() > 0 && !is_null($this->resource->first()->getResourceClass())) {
            $collect = $this->resource->first()->getResourceClass();
        }

        if (Str::endsWith(class_basename($this), 'Collection') &&
            class_exists($class = Str::replaceLast('Collection', '', get_class($this)))) {
            $collect = $class;
        }
        return $collect;
    }

    /**
     * @param Model|Collection|\Illuminate\Support\Collection $resource
     * @param array $relations
     */
    private function mapCollectionForRelations($resource, array $relations)
    {
        foreach ($relations as $relation) {
            $includeRelation = array_first(explode(".", $relation));


            if ($resource instanceof Model) {
                $this->includeRelation($includeRelation, $resource);
                $subRelations = $this->getItemRelations($resource, $includeRelation, $relations);
                $this->mapCollectionForRelations($resource, $subRelations);
            } elseif ($resource instanceof \Illuminate\Support\Collection) {
                $subRelations = [];
                if (!is_null($resource->pluck($includeRelation)->first())) {
                    $subRelations = $this->getItemRelations(
                        $resource->pluck($includeRelation)->first()->first(),
                        $includeRelation,
                        $relations
                    );
                }

                foreach ($resource->pluck($includeRelation) as $foundItems) {
                    $this->includeRelation($includeRelation, $foundItems);
                    $this->mapCollectionForRelations($foundItems, $subRelations);
                }
            }
        }
    }

    private function getItemRelations($resource, $currentRelation, $relations)
    {
        $subRelations = [];
        foreach ($relations as $value) {
            if (str_contains($value, $currentRelation) && str_contains($value, ".")) {
                $last = explode($currentRelation, $value);
                if (!empty($last)) {
                    $subRelations[] = substr(array_last($last), 1);
                }
            }
        }

        $persistentRelations = [];
        if (method_exists($resource, 'getPersistentRelations')) {
            $persistentRelations = $resource->getPersistentRelations();
        }

        $subRelations = array_unique(array_merge($subRelations, $persistentRelations));

        return $subRelations;
    }

    private function includeRelation($relation, $items)
    {
        if ($items instanceof Model) {
            $this->addIncludedItem($items);
        } elseif ($items instanceof Collection) {
            foreach ($items as $item) {
                $this->addIncludedItem($item);
            }
        }
    }

    /**
     * @param Model $item
     */
    private function addIncludedItem(Model $item)
    {
        $className = class_basename($item);
        if (!array_has($this->includes, $className . '.' . $item[$item->getKeyName()])) {
            $resourceClass = !is_null($item->getResourceClass()) ? $item->getResourceClass() : AbstractJsonApi::class;
            $this->includes[$className . '.' . $item[$item->getKeyName()]] = new $resourceClass($item);
        }
    }
}
