<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 25-03-18
 * Time: 19:39
 */

namespace Stefandebruin\JsonApi\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\JsonApi\Exceptions\UnknownModelType;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\JsonApiServiceProvider;
use \Illuminate\Http\Request;
use Stefandebruin\JsonApi\Service\ModelInformation;

class AbstractJsonApi extends Resource
{
    private $relations = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     * @throws UnknownModelType
     */
    public function toArray($request)
    {
        $type = array_search(get_class($this->resource), config('json-api.types'));
        if (empty($type)) {
            throw new UnknownModelType('Unknown type for model: ' . get_class($this->resource));
        }

        return [
            'type' => $type,
            'id' => (string)$this->resource->getKey(),
            'attributes' => $this->getResourceAttributes($request, $type),
            'links' => $this->getResourceLinks($type, $request),
            'relationships' => $this->getRelationships($this->resource)
        ];
    }

    /**
     * @param string $type
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function getResourceLinks($type, Request $request)
    {
        $links = [
            'self' => $this->resource->getUrl(),
        ];

        foreach ($this->getVisibleModelRelations() as $key => $relation) {
            $links[$key] = route($type . '.' . $key, $this->resource->getKey());
        }

        return $links;
    }

    public function with($request)
    {
        $this->buildInclude($this->resource, '');
        if (count($this->with) > 0) {
            return ['included' => array_values($this->with)];
        }

        return ['user-check' => \Auth::user()];
    }

    private function buildInclude($resource, $path)
    {
        $parameters = str_plural(strtolower(class_basename($resource)));
        $information = app()->make(ModelInformation::class);
        $modelRelations = $information->relations($resource);

//        var_dump(get_class($resource), $this->getRelationsResources($parameters));
        foreach ($this->getRelationsResources($parameters) as $relation) {
//            var_dump(get_class($resource), $relation, $path, $this->hasChildRelation($path.'.'.$relation));
            $path = trim($path.'.'.$relation, '.');
//            var_dump($path, $relation, $this->hasChildRelation($path));
//            echo '<br />';

            if (empty($relation)) {
                continue;
            }
            $resource->load($relation);
            $modelRelationData = $resource->getRelation($relation);
            if (!is_null($modelRelationData)) {
                if (!$modelRelations[$relation]['manyRelation']) {
                    if (!array_has($this->with, $relation . '.' . $modelRelationData->getKey())) {
                        $resourceClass = !is_null($modelRelationData->getResourceClass()) ?
                            $modelRelationData->getResourceClass() : AbstractJsonApi::class;
//                        echo $relation . '.' . $modelRelationData->getKey();

                        $this->with[$relation . '.' . $modelRelationData->getKey()] = new $resourceClass($modelRelationData, false);
                        if($this->hasChildRelation($path)){
                            $this->buildInclude($modelRelationData, $path);
                        }
                    }
                } else {
                    foreach ($modelRelationData as $relationItem) {
//                        var_dump($relation . '.' . $relationItem->getKey(), $path);
                        if (!array_has($this->with, $relation . '.' . $relationItem->getKey())) {
                            $resourceClass = !is_null($relationItem->getResourceClass()) ?
                                $relationItem->getResourceClass() : AbstractJsonApi::class;
                            $this->with[$relation . '.' . $relationItem->getKey()] = new $resourceClass($relationItem, false);
                            if($this->hasChildRelation($path)){
//                                echo 1;
                                $this->buildInclude($relationItem, $path);
                            }
                        }
                    }
                }
            }
        }
    }

    protected function getRelationships($resource)
    {
        /** @var ModelInformation $information */
        $relations = [];

        foreach ($this->getVisibleModelRelations($resource) as $key => $relation) {
            $data = [];
            if ($relation['manyRelation']) {
                foreach ($this->resource->$key as $item) {
                    $data[] = [
                        'type' => config('json-api.mapping.'.$key, $key),
                        'id' => strval($item->getKey())
                    ];
                }
            } else {
                $data = null;
                if (!is_null($this->resource->$key)) {
                    $data = [
                        'type' => config('json-api.mapping.'.$key, $key),
                        'id' => strval($this->resource->$key->getKey())
                    ];
                }
            }


            $relations[$key] = [
                'links' => [
                    'self' => route(
                        JsonApi::getResourceTypeByClass($resource) . '.relationship.' . $key,
                        $resource->getKey()
                    ),
                ],
                'data' => $data
            ];
        }

        return $relations;
    }

    /////////////////////

    public function withOLD($request)
    {
        $includes = [];
        $parameters = str_plural(strtolower(class_basename($this->resource)));

//        var_dump($this->getRelationsResources($parameters));
        $information = app()->make(ModelInformation::class);
        $modelRelations = $information->relations($this->resource);
        foreach ($this->getRelationsResources($parameters) as $relation) {
            if (empty($relation)) {
                continue;
            }

            $this->resource->load($relation);
            $modelRelationData = $this->resource->getRelation($relation);

            if (!is_null($modelRelationData)) {
                if (!$modelRelations[$relation]['manyRelation']) {
                    if (!array_has($includes, $relation . '.' . $modelRelationData->id)) {
                        $resourceClass = !is_null($modelRelationData->getResourceClass()) ?
                            $modelRelationData->getResourceClass() : AbstractJsonApi::class;
                        $includes[$relation . '.' . $modelRelationData->id] = new $resourceClass($modelRelationData, false);
                    }
                } else {
                    foreach ($modelRelationData as $relationItem) {
                        if (!array_has($includes, $relation . '.' . $relationItem->getKey())) {
                            $resourceClass = !is_null($relationItem->getResourceClass()) ?
                                $relationItem->getResourceClass() : AbstractJsonApi::class;
                            $includes[$relation . '.' . $relationItem->getKey()] = new $resourceClass($relationItem, false);
                        }
                    }
                }
            }
        }

        if (count($includes) > 0) {
            return ['included' => array_values($includes)];
        }

        return [];
    }

    protected function getResourceAttributes($request, $type)
    {
        if (!empty($request->query('fields'))) {
            $visibleFields = explode(',', array_get($request->query('fields'), $type));
            $fields = array_keys($this->resource->getAttributes());
            $hiddenFields = array_filter($fields, function ($item) use ($visibleFields) {
                return !in_array($item, $visibleFields);
            });
            $this->resource->addHidden($hiddenFields);
        }
        $this->resource->addHidden(array_keys($this->resource->getRelations()));
        $this->resource->addHidden('id');

        $translateble = property_exists($this->resource, 'translatable') ? $this->resource->translatable : [];

        $attributes = $this->resource->toArray();
        foreach ($attributes as $key => $attribute) {
            if (in_array($key, $translateble) && $request->has('locale')) {
                $attributes[$key] = array_get($attributes[$key], app()->getLocale(), "");
            }
        }

        foreach ($this->resource->getDates() as $dateColumn) {
            $date = $this->resource->$dateColumn;
            $attributes[$dateColumn] = !is_null($date) ? $date->toIso8601String() : '';
        }

        return $attributes;
    }

    protected function getRelationshipsOLD($request, $type)
    {
        if (is_null($this->relations)) {
            $parameters = str_plural(strtolower(class_basename($this->resource)));
            foreach ($this->getRelationsResources($parameters) as $relation) {
                if (!method_exists($this->resource, $relation)) {
                    continue;
                }

                $data = [];
                $this->resource->load($relation);

                $modelRelationData = $this->resource->getRelation($relation);
                $selfUrl = "";
                if (!is_null($modelRelationData)) {
                    if ($modelRelationData instanceof Model) {
                        $data = [
                            'type' => JsonApiServiceProvider::getResourceTypeByClass($modelRelationData),
                            'id' => (string)$modelRelationData[$modelRelationData->getKeyName()]
                        ];
                        $selfUrl = $modelRelationData->getUrl();
                    } else {
                        foreach ($this->resource->$relation as $phase) {
                            $data[] = [
                                'type' => $relation, 'id' => (string)$phase[$phase->getKeyName()]
                            ];
                        }
                        $selfUrl = route('poulesystem.' . $type . '.relationships.' . $relation, $this->resource->id);
                    }
                }

                $this->relations[$relation] = [
                    'links' => [
                        'self' => $selfUrl,
                    ],
                    'data' => $data,
                ];
            }
        }

        return $this->relations;
    }

    public function getRelationsResources($resource)
    {
        $include = explode(',', request()->get('include'));
        $includeMatch = [];

        if (!str_contains(request()->get('include'), $resource)) {
            foreach ($include as $item) {
                $includeMatch[$resource][] = array_first(explode(".", $item));
            }
        }
        foreach ($include as $item) {
            $a = explode('.', $item);
            $index = 0;
            $key = null;
            foreach ($a as $b) {
                if ($key === null) {
                    $key = $b;
                } else {
                    $includeMatch[$key][] = $b;
                    $key = $b;
                }
                $index++;
            }
        }

        foreach ($includeMatch as &$value) {
            $value = array_unique($value);
        }

        return array_get($includeMatch, $resource, []);
    }



    protected function getIncludeRelations($request)
    {
        return array_filter(
            explode(",",
                $request->get('include')
            ),
            function ($value) {
                return !empty($value);
            }
        );
    }

    private function getVisibleModelRelations($resource = null)
    {
        $information = app()->make(ModelInformation::class);
        $relations = [];

        $resource = is_null($resource) ? $this->resource : $resource;

        foreach ($information->relations($resource) as $key => $relation) {
            if (!in_array($key, $this->resource->getHiddenRelations())) {
                $relations[$key] = $relation;
            }
        }

        return $relations;
    }

    private function hasChildRelation($relation){
        $include = explode(',', request()->get('include'));
        $filter = array_filter($include, function($item) use($relation) { return starts_with($item, $relation.'.'); });
        return count($filter) > 0;
    }

    private function getChildRelations($includePath)
    {
        $include = explode(',', request()->get('include'));
        $filter = array_filter($include, function($item) use($includePath) { return starts_with($item, $includePath.'.'); });

        var_dump($filter);
    }
}
