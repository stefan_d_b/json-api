<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 14/10/2018
 * Time: 17:12
 */

namespace Stefandebruin\JsonApi\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Stefandebruin\JsonApi\Facades\JsonApi;

class JsonRelationShip extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->resource->getKey(),
            'type' => 'model TYPE',
        ];
    }
}
