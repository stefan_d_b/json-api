<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 14/10/2018
 * Time: 17:12
 */

namespace Stefandebruin\JsonApi\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class JsonRelationShipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $links = [
            'self' => $request->url()
        ];

        return [
            'data' => $this->collection,
            'links' => $links,
        ];
    }
}
