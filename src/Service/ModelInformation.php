<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 16-09-18
 * Time: 19:40
 */

namespace Stefandebruin\JsonApi\Service;

use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;

class ModelInformation
{
    private $toOneRelation = [
        'hasOne',
        'HasOne',
        'BelongsTo',
        'morphTo',
    ];

    private $hideFunctions = [
        'getUrl'
    ];

    public function relations($model)
    {
        if (is_string($model)) {
            $model = app()->make($model);
        }
        $relationships = [];

        try {
            foreach ((new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                if ($method->class != get_class($model) ||
                    !empty($method->getParameters()) ||
                    $method->getName() == __FUNCTION__) {
                    continue;
                }

                if (in_array($method->getName(), $this->hideFunctions)) {
                    continue;
                }

                $return = $method->invoke($model);

                if ($return instanceof Relation) {
                    $type = (new ReflectionClass($return))->getShortName();
                    $relationships[$method->getName()] = [
                        'model' => (new ReflectionClass($return->getRelated()))->getName(),
                        'type' => $type,
                        'manyRelation' => !in_array($type, $this->toOneRelation)
                    ];
                }
            }
        } catch (\ReflectionException $e) {
            dd($e->getMessage());
        }
        return $relationships;
    }

    public function attributes()
    {
    }
}
