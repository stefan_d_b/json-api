<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 05-07-18
 * Time: 20:00
 */

namespace Stefandebruin\JsonApi\Service;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;

class FilterCollectionService
{
    private $addedRelations = [];

    /**
     * @param Model $model
     * @return Model
     * @throws \Exception
     */
    public function filter(&$model)
    {
        $request = request();

        $relations = $model->relationships();
        $query = $model->newQuery();

        foreach ($request->query('filter', []) as $columnName => $filterValue) {
            $relationInfo = null;
            if (str_contains($columnName, '.')) {
                list($relation) = explode('.', $columnName);
                /** @var Relation $relationInfo */
                $relationInfo = $model->$relation();
                $fucntion = 'join' . $relations[$relation]['type'];
                $this->$fucntion($relationInfo, $model, $query);
            }


            $column = $this->getColumnName($columnName, $relationInfo);

            if (!is_array($filterValue)) {
                $query = $query->where($column, $filterValue);
            } else {
                switch (key($filterValue)) {
                    case 'in':
                        $query = $query->whereIn($column, $filterValue['in']);
                        break;
                    case 'nin':
                        $query = $query->whereNotIn($column, $filterValue['nin']);
                        break;
                    case 'gt':
                        $query = $query->where($column, '>', $filterValue['gt']);
                        break;
                    case 'gteq':
                        $query = $query->where($column, '>=', $filterValue['gteq']);
                        break;
                    case 'lt':
                        $query = $query->where($column, '<', $filterValue['lt']);
                        break;
                    case 'lteq':
                        $query = $query->where($column, '<=', $filterValue['lteq']);
                        break;
                }
            }
        }

        return $query;
    }


    private function getColumnName($column, $relation)
    {
        $table = "";
        switch (get_class($relation)) {
            case BelongsToMany::class:
                $table = $relation->getTable();
                break;
        }

        if (strlen($table) > 0) {
            $table .= '.';
        }
        $column = str_contains($column, '.') ? explode('.', $column)[1] : $column;
        return $table . trim($column, '.');
    }

    /**
     * @param BelongsToMany $relation
     * @param Model $model
     */
    private function joinBelongsToMany(BelongsToMany $relation, Model $model, Builder $query)
    {
        if (!in_array($relation->getTable(), $this->addedRelations)) {
            $query->join($relation->getTable(), $model->getKeyName(), $relation->getForeignPivotKeyName());
            $this->addedRelations[] = $relation->getTable();
        }
    }
}
