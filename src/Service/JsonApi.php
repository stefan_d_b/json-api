<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 09-09-18
 * Time: 21:25
 */

namespace Stefandebruin\JsonApi\Service;

/**
 * Class JsonApi
 * @package Stefandebruin\JsonApi\Service
 */
class JsonApi
{
    /**
     * @param null $path
     * @param string $delimiter
     * @return mixed
     */
    public function getResourceType($path = null, $delimiter = '/')
    {
        $urlPath = !is_null($path) ? $path : request()->path();
        $split = array_filter(
            explode($delimiter, $urlPath),
            function ($value) {
                return !is_numeric($value) && $value !== 'bulk';
            }
        );
        return end($split);
    }

    /**
     * @param $resource
     * @return mixed
     */
    public function getResourceClass($resource)
    {
        return array_get(config('json-api.types'), $resource, null);
    }

    /**
     * @param $class
     * @return false|int|string
     */
    public function getResourceTypeByClass($class)
    {
        return array_search(get_class($class), config('json-api.types'));
    }
}
