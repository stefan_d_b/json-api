<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 09-05-18
 * Time: 21:38
 */

namespace Stefandebruin\JsonApi\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class UpdatedOnly implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (request()->has('LastChanges')) {
            $builder->where('updated_at', '>=', '2018-05-01 00:00:00');
        }
    }
}
