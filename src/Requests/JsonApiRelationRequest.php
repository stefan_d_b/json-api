<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 31-03-18
 * Time: 22:24
 */

namespace Stefandebruin\JsonApi\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Stefandebruin\JsonApi\Exceptions\ValidationException;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Rules\Connected;
use Stefandebruin\JsonApi\Rules\RelationRecourseClass;
use Stefandebruin\JsonApi\Rules\RelationType;
use Stefandebruin\JsonApi\Rules\ValueType;
use Stefandebruin\JsonApi\Service\ModelInformation;

class JsonApiRelationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $modelInformation = app()->make(ModelInformation::class);

        $uri = explode("/", $this->getRequestUri());
        $reverseUri = array_reverse($uri);

        $relation = $reverseUri[0];
        $resource = JsonApi::getResourceClass($reverseUri[3]);

        $rules = [
            'data' => [
                'present',
                'nullable',
                new ValueType(['NULL', 'array']),
                new RelationType(app()->make($resource), $relation),
            ]
        ];

        $relations = $modelInformation->relations($resource);
        $currentRelation = array_get($relations, $relation);

        if (is_array($this->input('data')) && count($this->input('data')) > 0) {
            $fieldName = "data.";
            if ($currentRelation['manyRelation']) {
                $fieldName .= '*.';
            }
            $rules[$fieldName . 'type'] = ['string', new RelationRecourseClass($relation)];
            $relationClass = app()->make(JsonApi::getResourceClass($relation));
            $rules[$fieldName . 'id'] = ['string', 'exists:' . $relationClass->getTable() . ',id'];
            if ($this->getMethod() === "DELETE") {
                $model = app()->make($resource);
                $parameterName = strtolower(class_basename($resource));
                $parameters = $this->route()->parameters;
                $value = array_get($parameters, $parameterName) ?? array_get($parameters, $parameterName . '_id');
                $model = $model->find($value);
                $rules[$fieldName . 'id'][] = new Connected($model, $relation);
            }
        }

        return $rules;
    }

//    public function getRowRule()
//    {
//        $resource = JsonApi::getResourceType();
//        var_dump($resource);
//        $model = app()->make(JsonAPi::getResourceClass($resource));
//        return [
//            'type' => ['required', Rule::in([$resource])],
//            'id' => [
//                'required',
//                'exists:'.$model->getTable().','.$model->getKeyName()
//            ],
//        ];
//    }
//
//    /**
//     * get all the posted ids to connect
//     * @return array
//     */
//    public function getRelationIds(): array
//    {
//        $resource = \Stefandebruin\JsonApi\JsonApiServiceProvider::getResourceType();
//        $ids = [];
//        foreach ($this->input('data') as $row) {
//            if ($row['type'] === $resource) {
//                $ids[] = $row['id'];
//            }
//        }
//        return $ids;
//    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
