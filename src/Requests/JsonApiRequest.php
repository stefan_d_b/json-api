<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 27-03-18
 * Time: 20:43
 */

namespace Stefandebruin\JsonApi\Requests;

use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Stefandebruin\JsonApi\Exceptions\ValidationException;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Illuminate\Foundation\Http\FormRequest;
use Stefandebruin\JsonApi\Rules\RequiredIfMethod;
use Stefandebruin\JsonApi\Service\ModelInformation;

/**
 * @property boolean bulkAction
 */
class JsonApiRequest extends FormRequest
{
    protected $modelType = null;

    /**
     * @var ModelInformation
     */
    private $modelInformation;

    public function __construct(
        array $query = array(),
        array $request = array(),
        array $attributes = array(),
        array $cookies = array(),
        array $files = array(),
        array $server = array(),
        $content = null
    )
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->modelInformation = app()->make(ModelInformation::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = !is_null($this->modelType) ? $this->modelType : JsonApi::getResourceType();
        $rules = [
            'data' => ['required', 'array'],
        ];

        $prefix = 'data.';
        if ($this->isBulkRequest()) {
            $prefix = 'data.*.';
            $rules['data.*.type'] = ['required', Rule::in([$type])];
            $rules['data.*.attributes'] = ['required', 'array'];
            $rules['data.*.id'] = ['string'];
        } else {
            $rules['data.type'] = ['required', Rule::in([$type])];
            $rules['data.attributes'] = ['required', 'array'];
        }

        foreach ($this->attributeRules() as $key => $value) {
            $attributeRules = [new RequiredIfMethod($this->getMethod(), ['post'])];
            $rules[$prefix . 'attributes.' . $key] = array_merge($attributeRules, $value);
        }

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules[$prefix . 'id'] = ['required', 'string', 'resourceId'];
        } elseif ($this->method() === 'POST' && !$this->isBulkRequest()) {
            $rules[$prefix . 'id'] = ['notAllowed'];
        }

        if ($this->has('data.relationships')) {
            $rules['data.relationships.*'] = ['required', 'array', 'testValidation'];

            $resourceModel = app()->make(JsonApi::getResourceClass($type));
            $relations = $this->modelInformation->relations($resourceModel);
            $relationsKeys = array_keys($relations);
            $resources = (array_keys(Config::get('json-api.types')));
            foreach ($this->input('data.relationships') as $relationShip => $relationData) {
                if (array_key_exists($relationShip, $relations)) {
                    $index = str_contains($relations[$relationShip]['type'], 'Many') ? '.*.' : '.';
                    $rules['data.relationships.' . $relationShip . '.data' . $index . 'type'] = [
                        'required',
                        Rule::in($resources)
                    ];
                    if (in_array($relationShip, $resources)) {
                        $m = app()->make(Config::get('json-api.types.' . $relationShip));
                        $rules['data.relationships.' . $relationShip . '.data' . $index . 'id'] = [
                            'required',
                            'exists:' . $m->getTable() . ',id'
                        ];
                    }
                }
            }
        }

        return $rules;
    }

    public function attributeRules()
    {
        return [];
    }

    public function allAttributes()
    {
        return $this->input('data.attributes');
    }

    private function isBulkRequest()
    {
        if (property_exists($this, 'bulkAction')) {
            return $this->bulkAction;
        }
        return ends_with(class_basename($this), 'BulkRequest');
    }

    protected $jsonResponse = false;

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
