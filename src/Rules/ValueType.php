<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 24-09-18
 * Time: 21:51
 */

namespace Stefandebruin\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValueType implements Rule
{
    /**
     * @var array
     */
    private $allowedTypes;

    private $foundType = false;

    private $attribute;
    /**
     * ValueType constructor.
     * @param array $allowedTypes
     */
    public function __construct(array $allowedTypes)
    {
        $this->allowedTypes = $allowedTypes;
    }

    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $this->foundType = gettype($value);
        return in_array($this->foundType, $this->allowedTypes);
    }

    public function message()
    {
        if ($this->foundType !== false) {
            return sprintf('The %s field can not be a %s.', $this->attribute, $this->foundType);
        }
        return 'The only allowed types are: ' . implode(",", $this->allowedTypes);
    }
}
