<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 24-09-18
 * Time: 21:51
 */

namespace Stefandebruin\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;
use Stefandebruin\JsonApi\Service\ModelInformation;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class RelationType implements Rule
{
    /**
     * @var ModelInformation
     */
    private $modelInformation;
    /**
     * @var EloquentModel
     */
    private $resource;
    private $relation;

    private $currentRelation = null;

    /**
     * ValueType constructor.
     * @param EloquentModel $resource
     * @param $relation
     */
    public function __construct(EloquentModel $resource, $relation)
    {
        $this->modelInformation = app()->make(ModelInformation::class);
        $this->resource = $resource;
        $this->relation = $relation;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            return true;
        }

        $relations = $this->modelInformation->relations($this->resource);
        $this->currentRelation = array_get($relations, $this->relation);

        $test = false;
        if ($this->currentRelation['manyRelation']) {
            if (!array_has($value, 'type') && !array_has($value, 'id')) {
                $test = true;
            }
        } elseif (!$this->currentRelation['manyRelation']) {
            if (array_has($value, 'type') && array_has($value, 'id')) {
                $test = true;
            }
        }

        return $test;
    }

    /**
     * @inheritdoc
     */
    public function message()
    {
        if ($this->currentRelation['manyRelation']) {
            return sprintf(__('%s relation is not a belongs to relation'), ucwords($this->relation));
        } else {
            return sprintf(__('%s relation is not a (to) many relation'), ucwords($this->relation));
        }
    }
}
