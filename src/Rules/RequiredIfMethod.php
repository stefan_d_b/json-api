<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 16/10/2018
 * Time: 19:00
 */

namespace Stefandebruin\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfMethod implements Rule
{
    /**
     * @var array
     */
    private $methods;

    private $attribute;
    private $currentMethod;

    /**
     * RequiredIfMethod constructor.
     * @param $currentMethod
     * @param array $methods
     */
    public function __construct($currentMethod, $methods = [])
    {
        $this->currentMethod = $currentMethod;
        $this->methods = $methods;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        if (count($this->methods) === 0) {
            return true;
        }

        if (in_array(strtolower($this->currentMethod), $this->methods) && empty($value)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $method = $this->currentMethod === 'POST' ? 'creating' : 'updating';
        return sprintf(
            __('%s is required by %s this resource'),
            $this->attribute,
            $method
        );
    }
}
