<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 24-09-18
 * Time: 21:51
 */

namespace Stefandebruin\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Service\ModelInformation;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class RelationRecourseClass implements Rule
{
    /**
     * @var
     */
    private $relation;

    /**
     * @var
     */
    private $attributeKey;

    /**
     * ValueType constructor.
     * @param $relation
     */
    public function __construct($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attributeKey = $attribute;
        return !is_null($value) && JsonApi::getResourceClass($value) == JsonApi::getResourceClass($this->relation);
    }

    public function message()
    {
        return $this->attributeKey . ' is not a valid relation type';
    }
}
