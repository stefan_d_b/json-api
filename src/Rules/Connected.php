<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 24-09-18
 * Time: 21:51
 */

namespace Stefandebruin\JsonApi\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class Connected implements Rule
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var string
     */
    private $relation;

    private $attributeKey;

    /**
     * Connected constructor.
     * @param Model $model
     * @param $relation
     */
    public function __construct(Model $model, $relation)
    {
        $this->model = $model;
        $this->relation = $relation;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attributeKey = $attribute;
        $relationData = $this->model->{$this->relation};
        return $relationData ? $relationData->contains($value) : false;
    }

    public function message()
    {
        return sprintf(__('%s is not connected'), $this->attributeKey);
    }
}
