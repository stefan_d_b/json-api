<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 21:20
 */

namespace Stefandebruin\JsonApi\Middleware\Parameters;

use Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SortParameter
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     * @throws Exception
     */
    public function handle(\Illuminate\Http\Request $request)
    {
        $sort = $request->get('sort');

        preg_match("/^[-]?[0-9a-zA-Z,]+$/", $sort, $validated);
        if (!array_has($validated, 0) || $validated[0] != $sort) {
            throw new BadRequestHttpException("invalid characters used");
        }

        return true;
    }
}
