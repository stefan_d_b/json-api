<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 21:20
 */

namespace Stefandebruin\JsonApi\Middleware\Parameters;

use Exception;
use Illuminate\Support\Facades\Cache;
use Stefandebruin\JsonApi\Exceptions\BadIncludeException;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Service\ModelInformation;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class IncludeParameter
{
    /**
     * @var ModelInformation
     */
    private $modelInformation;

    /**
     * IncludeParameter constructor.
     * @param ModelInformation $modelInformation
     */
    public function __construct(ModelInformation $modelInformation)
    {
        $this->modelInformation = $modelInformation;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     * @throws Exception
     */
    public function handle(\Illuminate\Http\Request $request)
    {
        if (empty($request->get('include'))) {
            return true;
        }
        $resource = JsonApi::getResourceType($request->path());
        $resourceClass = JsonApi::getResourceClass($resource);

        if (is_null($resourceClass)) {
            throw new Exception('No mapping found for data type: ' . $resource);
        }

        $resourceClass = app()->make($resourceClass);
        $mainResourceClass = clone $resourceClass;

        $mainResource = $resource;

        preg_match("/^[0-9a-zA-Z_,.]+$/", $request->get('include'), $validated);
        if (!array_has($validated, 0) || $validated[0] != $request->get('include')) {
            throw new BadRequestHttpException("invalid characters used");
        }

        foreach (explode(',', $request->get('include')) as $item) {
            $methods = Cache::rememberForever('model.relations.'.$resource, function () use ($resourceClass) {
                return $this->modelInformation->relations($resourceClass);
            });
            foreach (explode(".", $item) as $resource) {
                if (!array_has($methods, $resource)) {
//                    throw new BadIncludeException(
//                        ['parameter' => 'include'],
//                        "Invalid Query Parameter",
//                        'The resource does not have an `' . $resource . '` relationship path.'
//                    );
                }

                $resourceClass = app()->make(array_get(config('json-api.types'), $resource, false));
            }
            $resourceClass = $mainResourceClass;
            $resource = $mainResource;
        }

        return true;
    }
}
