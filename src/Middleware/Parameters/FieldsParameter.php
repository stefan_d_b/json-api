<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 21:20
 */

namespace Stefandebruin\JsonApi\Middleware\Parameters;

use Exception;
use Stefandebruin\JsonApi\Facades\JsonApi;
use Stefandebruin\JsonApi\Service\ModelInformation;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FieldsParameter
{
    /**
     * @var ModelInformation
     */
    private $modelInformation;

    /**
     * FieldsParameter constructor.
     * @param ModelInformation $modelInformation
     */
    public function __construct(ModelInformation $modelInformation)
    {
        $this->modelInformation = $modelInformation;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     * @throws Exception
     */
    public function handle(\Illuminate\Http\Request $request)
    {
        $fields = $request->get('fields');
        if (empty($fields)) {
            return true;
        }

        foreach ($fields as $resource => $attributes) {
            preg_match("/^[0-9a-zA-Z]+$/", $resource, $validatedRecourse);
            if (!array_has($validatedRecourse, 0) || $validatedRecourse[0] != $resource) {
                throw new BadRequestHttpException("invalid characters used");
            }

            preg_match("/^[0-9a-zA-Z]+$/", $attributes, $validatedAttributes);
            if (!array_has($validatedAttributes, 0) || $validatedAttributes[0] != $attributes) {
                throw new BadRequestHttpException("invalid characters used");
            }

            if (is_null(JsonApi::getResourceClass($resource))) {
                throw new BadRequestHttpException("model not configured");
            }
        }
        return true;
    }
}
