<?php
/**
 * Created by PhpStorm.
 * User: sfdebruin
 * Date: 13-09-18
 * Time: 19:22
 */

namespace Stefandebruin\JsonApi\Middleware;

use Closure;
use Stefandebruin\JsonApi\Middleware\Parameters\FieldsParameter;
use Stefandebruin\JsonApi\Middleware\Parameters\FilterParameter;
use Stefandebruin\JsonApi\Middleware\Parameters\IncludeParameter;
use Stefandebruin\JsonApi\Middleware\Parameters\SortParameter;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class JsonApi
{
    private $mapping = [
        'include' => IncludeParameter::class,
        'fields' => FieldsParameter::class,
        'sort' => SortParameter::class,
        'filter' => FilterParameter::class,
        'page' => FilterParameter::class,
    ];

    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param array $params
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next, ...$params)
    {
        if (!$request->hasHeader('Content-Type') ||
            !starts_with($request->header('Accept'), 'application/vnd.api+json')) {
            throw new UnsupportedMediaTypeHttpException('Wrong headers');
        }
        if ($request->getMethod() === "POST" && (!$request->hasHeader('Accept') ||
                !starts_with($request->header('Content-Type'), 'application/vnd.api+json'))
        ) {
            throw new UnsupportedMediaTypeHttpException('Wrong headers');
        }

        //change the localisation language
        $locale = $request->query('locale', null);
        if (!is_null($locale)) {
            if (!in_array($locale, config('json-api.locale_options'))) {
                throw new BadRequestHttpException('Language not configure');
            }
            app()->setLocale($locale);
        }

        foreach ($request->query() as $key => $value) {
            if ($key === 'locale') {
                continue;
            }
            if (!in_array($key, $params) && $key !== 'page' || !array_key_exists($key, $this->mapping) && $key !== 'page') {
                throw new BadRequestHttpException();
            } else {
                $class = app()->make($this->mapping[$key]);
                $check = $class->handle($request);
                if (!$check) {
                    throw new BadRequestHttpException();
                }
            }
        }

        $response = $next($request);
        $response->headers->set('Accept', 'application/vnd.api+json');
        return $response;
    }
}
